# PaymentService


## What is been used?
- .Net Core 2.2
- WebAPI
- EF Core
- AutoMapper
- Swagger
- Unit Tests (using xUnit, Moq, and Fixture)

## How to run?
- Install .NET CORE 2.2

## Use this commands to restore and run the code:

- dotnet restore
- dotnet run

## Post Request

```csharp
POST /api/Payments HTTP/1.1
Host: localhost:5001
Content-Type: application/json

{
  "FirstName": "Mia",
  "LastName": "Bean",
  "CardNumber": "41970138204188854",
  "Flag": "VISA",
  "expiryMonth": 10,
  "expiryYear": 2020,
  "CVV":"168",
  "Address":"2153 Curabitur Avenue"
}
```