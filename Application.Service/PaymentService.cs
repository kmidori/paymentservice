﻿namespace Application.Service
{
    using System;
    using System.Threading.Tasks;
    using Application.DTO;
    using AutoMapper;
    using Data.Gateway;
    using Data.Repository;
    using Domain.Model;

    public class PaymentService : IPaymentService
    {
        private readonly IMapper mapper;

        private readonly IPaymentRepository paymentRepository;

        private readonly IBankClient bankClient;
        
        public PaymentService(IMapper mapper, IPaymentRepository paymentRepository, IBankClient bankClient)
        {
            this.mapper = mapper;
            this.paymentRepository = paymentRepository;
            this.bankClient = bankClient;
        }

        public async Task<PaymentResponseDto> GetAsync(Guid id)
        {
            var result = await this.paymentRepository.GetAsync(id);

            return this.mapper.Map<PaymentResponseDto>(result);
        }

        public async Task<PaymentResponseDto> ProcessAsync(Payment payment)
        {
            var paymentWithoutSensitiveData = new PaymentWithoutSensitiveData(payment).GetPayment();

            await this.paymentRepository.AddAsync(paymentWithoutSensitiveData);

            var processResponse = await this.bankClient.AddPaymentAsync(payment);

            var paymentUpdated = await this.UpdatePaymentWithProcessResponse(paymentWithoutSensitiveData, processResponse);

            var paymentResponseDto = this.mapper.Map<PaymentResponseDto>(paymentUpdated);

            return paymentResponseDto;
        }
        
        private async Task<Payment> UpdatePaymentWithProcessResponse(Payment payment, ProcessPaymentResponseDTO processResponse)
        {
            Enum.TryParse(processResponse.Status, out PaymentStatus paymentStatus);
            payment.Status = paymentStatus;
            payment.ExternalId = processResponse.Id;

            await this.paymentRepository.UpdateAsync(payment);

            return payment;
        }
    }
}
