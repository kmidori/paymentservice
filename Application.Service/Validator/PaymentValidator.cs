﻿namespace Application.Service.Validator
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using Application.DTO;

    public class PaymentValidator : IPaymentValidator
    {
        public List<string> ErrorList { get; set; }

        public PaymentRequestDto PaymentRequest { get; set; }

        private readonly Regex numberCheck = new Regex("^[0-9]*$");

        public PaymentValidator(PaymentRequestDto paymentRequest)
        {
            this.PaymentRequest = paymentRequest;
            this.ErrorList = new List<string>();
        }

        public bool IsValid()
        {
            return this.ValidateCreditCardNumber() &&
                   this.ValidateCVV() &&
                   this.ValidateExpiryMonth() &&
                   this.ValidateExpiryYear();
        }

        private bool ValidateCreditCardNumber()
        {
            if (this.numberCheck.IsMatch(this.PaymentRequest.CardNumber))
                return true;

            this.ErrorList.Add("Invalid credit card number");
            return false;
        }

        private bool ValidateCVV()
        {
            if (this.PaymentRequest.CVV > 0)
                return true;

            this.ErrorList.Add("Invalid CVV");
            return false;
        }

        private bool ValidateExpiryYear()
        {
            if (this.PaymentRequest.ExpiryYear > 0)
                return true;

            this.ErrorList.Add("Invalid Expiry Year");
            return false;
        }

        private bool ValidateExpiryMonth()
        {
            if (this.PaymentRequest.ExpiryMonth > 0)
                return true;

            this.ErrorList.Add("Invalid Expiry Month");
            return false;
        }
    }
}
