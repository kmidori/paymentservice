﻿namespace Application.Service.Validator
{
    using System.Collections.Generic;
    using Application.DTO;
    
    public interface IPaymentValidator
    {
        List<string> ErrorList { get; set; }

        PaymentRequestDto PaymentRequest { get; set; }

        bool IsValid();
    }
}
