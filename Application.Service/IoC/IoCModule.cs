﻿namespace Application.Service.IoC
{
    using System.Diagnostics.CodeAnalysis;
    using Application.Service.Validator;
    using Autofac;

    [ExcludeFromCodeCoverage]
    public class IoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PaymentService>().As<IPaymentService>();
            builder.RegisterType<PaymentValidator>().As<IPaymentValidator>();
        }
    }
}
