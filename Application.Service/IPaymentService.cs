﻿namespace Application.Service
{
    using System;
    using System.Threading.Tasks;
    using Application.DTO;
    using Domain.Model;

    public interface IPaymentService
    {
        Task<PaymentResponseDto> ProcessAsync(Payment payment);
        
        Task<PaymentResponseDto> GetAsync(Guid id);
    }
}
