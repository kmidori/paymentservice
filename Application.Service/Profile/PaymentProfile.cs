﻿namespace Application.Service.Profile
{
    using Application.DTO;
    using AutoMapper;
    using Data.Gateway;
    using Domain.Model;

    public class PaymentProfile : Profile
    {
        public PaymentProfile()
        {
            CreateMap<Payment, PaymentRequestDto>();
            CreateMap<PaymentRequestDto, Payment>();

            CreateMap<Payment, PaymentResponseDto>();
            CreateMap<PaymentResponseDto, Payment>();

            CreateMap<Payment, ProcessPaymentRequestDTO>();  
            
        }
    }
}
