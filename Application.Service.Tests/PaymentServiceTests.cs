namespace Application.Service.Tests
{
    using System;
    using System.Threading.Tasks;
    using Application.Service.Profile;
    using AutoFixture;
    using AutoMapper;
    using Data.Gateway;
    using Data.Repository;
    using Domain.Model;
    using Moq;
    using Xunit;

    public class PaymentServiceTests
    {
        private readonly Mock<IBankClient> bankClientMock;

        private readonly Fixture fixture;

        private readonly IMapper mapper;

        private readonly Mock<IPaymentRepository> paymentRepositoryMock;

        private readonly PaymentService paymentService;

        public PaymentServiceTests()
        {
            this.fixture = new Fixture();
            this.paymentRepositoryMock = new Mock<IPaymentRepository>();
            this.bankClientMock = new Mock<IBankClient>();

            var config = new MapperConfiguration(cfg => { cfg.AddProfile<PaymentProfile>(); });
            this.mapper = config.CreateMapper();

            this.paymentService = new PaymentService(
                this.mapper,
                this.paymentRepositoryMock.Object,
                this.bankClientMock.Object);
        }

        [Fact]
        public async Task AddPaymentAsync_WithValidPayment_ShouldCallClient()
        {
            // Arrange
            var payment = this.fixture.Build<Payment>().With(w => w.CardNumber, "41970138204188855").Create();
            var processPaymentResponseDto = this.fixture.Create<ProcessPaymentResponseDTO>();

            this.bankClientMock.Setup(s => s.AddPaymentAsync(It.IsAny<Payment>()))
                .ReturnsAsync(processPaymentResponseDto);
            this.paymentRepositoryMock.Setup(s => s.AddAsync(It.IsAny<Payment>())).ReturnsAsync(true);

            // Act
            await this.paymentService.ProcessAsync(payment);

            // Assert
            this.bankClientMock.Verify(v => v.AddPaymentAsync(It.IsAny<Payment>()), Times.Once);
        }

        [Fact]
        public async Task AddPaymentAsync_WithValidPayment_ShouldCallRepositoryAddAsync()
        {
            // Arrange
            var payment = this.fixture.Build<Payment>().With(w => w.CardNumber, "41970138204188855").Create();
            var processPaymentResponseDto = this.fixture.Create<ProcessPaymentResponseDTO>();

            this.bankClientMock.Setup(s => s.AddPaymentAsync(It.IsAny<Payment>()))
                .ReturnsAsync(processPaymentResponseDto);
            this.paymentRepositoryMock.Setup(s => s.AddAsync(payment)).ReturnsAsync(true);

            // Act
            await this.paymentService.ProcessAsync(payment);

            // Assert
            this.paymentRepositoryMock.Verify(v => v.AddAsync(It.IsAny<Payment>()), Times.Once);
        }

        [Fact]
        public async Task AddPaymentAsync_WithValidPayment_ShouldCallRepositoryUpdateAsync()
        {
            // Arrange
            var payment = this.fixture.Build<Payment>().With(w => w.CardNumber, "41970138204188855").Create();
            var processPaymentResponseDto = this.fixture.Create<ProcessPaymentResponseDTO>();

            this.bankClientMock.Setup(s => s.AddPaymentAsync(It.IsAny<Payment>()))
                .ReturnsAsync(processPaymentResponseDto);
            this.paymentRepositoryMock.Setup(s => s.AddAsync(It.IsAny<Payment>())).ReturnsAsync(true);

            // Act
            await this.paymentService.ProcessAsync(payment);

            // Assert
            this.paymentRepositoryMock.Verify(v => v.UpdateAsync(It.IsAny<Payment>()), Times.Once);
        }

        [Fact]
        public async Task AddPaymentAsync_WithValidPayment_ShouldCallUpdateStatus()
        {
            // Arrange
            var status = "Rejected";
            var payment = this.fixture.Build<Payment>().With(w => w.CardNumber, "41970138204188855").Create();
            var paymentProcessDto = new ProcessPaymentResponseDTO { Status = status };

            this.bankClientMock.Setup(s => s.AddPaymentAsync(It.IsAny<Payment>())).ReturnsAsync(paymentProcessDto);
            this.paymentRepositoryMock.Setup(s => s.AddAsync(payment)).ReturnsAsync(true);

            // Act
            await this.paymentService.ProcessAsync(payment);

            // Assert
            this.paymentRepositoryMock.Verify(
                v => v.UpdateAsync(It.Is<Payment>(p => p.Status.ToString() == status)),
                Times.Once);
        }

        [Fact]
        public async Task AddPaymentAsync_WithValidPayment_ShouldHideSensitiveData()
        {
            // Arrange
            var cardNumber = "4009652773336764";
            var payment = this.fixture.Build<Payment>().With(w => w.CardNumber, cardNumber).Create();
            var paymentProcessDto = this.fixture.Create<ProcessPaymentResponseDTO>();
            var expectedCardNumber = "************6764";

            this.bankClientMock.Setup(s => s.AddPaymentAsync(It.IsAny<Payment>())).ReturnsAsync(paymentProcessDto);
            this.paymentRepositoryMock.Setup(s => s.AddAsync(payment)).ReturnsAsync(true);

            // Act
            await this.paymentService.ProcessAsync(payment);

            // Assert
            this.paymentRepositoryMock.Verify(
                v => v.AddAsync(It.Is<Payment>(p => p.CardNumber.ToString() == expectedCardNumber)),
                Times.Once);
        }

        [Fact]
        public async Task AddPaymentAsync_WithValidPayment_ShouldReturnNotNull()
        {
            // Arrange
            var payment = this.fixture.Build<Payment>().With(w => w.CardNumber, "41970138204188855").Create();
            var processPaymentResponseDto = this.fixture.Create<ProcessPaymentResponseDTO>();

            this.bankClientMock.Setup(s => s.AddPaymentAsync(It.IsAny<Payment>()))
                .ReturnsAsync(processPaymentResponseDto);
            this.paymentRepositoryMock.Setup(s => s.AddAsync(payment)).ReturnsAsync(true);

            // Act
            var result = await this.paymentService.ProcessAsync(payment);

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public async Task AddPaymentAsync_WithValidPayment_ShouldUpdateExternalId()
        {
            // Arrange
            var externalId = this.fixture.Create<Guid>();
            var payment = this.fixture.Build<Payment>().With(w => w.CardNumber, "41970138204188855").Create();
            var paymentProcessDto = new ProcessPaymentResponseDTO { Id = externalId };

            this.bankClientMock.Setup(s => s.AddPaymentAsync(It.IsAny<Payment>())).ReturnsAsync(paymentProcessDto);
            this.paymentRepositoryMock.Setup(s => s.AddAsync(payment)).ReturnsAsync(true);

            // Act
            await this.paymentService.ProcessAsync(payment);

            // Assert
            this.paymentRepositoryMock.Verify(
                v => v.UpdateAsync(It.Is<Payment>(p => p.ExternalId == externalId)),
                Times.Once);
        }

        [Fact]
        public async Task GetAsync_WithValidPayment_ShouldCallRepositoryGetAsync()
        {
            // Arrange
            var id = this.fixture.Create<Guid>();

            // Act
            await this.paymentService.GetAsync(id);

            // Assert
            this.paymentRepositoryMock.Verify(v => v.GetAsync(id), Times.Once);
        }
    }
}