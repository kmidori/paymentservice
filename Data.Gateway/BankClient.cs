﻿namespace Data.Gateway
{
    using System;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using AutoMapper;
    using Domain.Model;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using JsonSerializer = System.Text.Json.JsonSerializer;

    public class BankClient : IBankClient
    {
        private readonly HttpClient client;

        private readonly IMapper mapper;
        
        private readonly bool toggleActiveMock;

        private readonly string endpoint;

        public BankClient(HttpClient client, IMapper mapper, IConfigurationSection clientConfiguration)
        {
            this.client = client;
            this.mapper = mapper;
            this.toggleActiveMock = bool.Parse(clientConfiguration["toggleActiveMock"]);
            this.endpoint = clientConfiguration["endpoint"];
        }
        
        public async Task<ProcessPaymentResponseDTO> AddPaymentAsync(Payment payment)
        {
            if (this.toggleActiveMock)
            {
                return this.MockResponseFromClient();
            }

            var processPaymentRequestDto = this.mapper.Map<ProcessPaymentRequestDTO>(payment);

            var response = await this.client.PostAsync(this.endpoint, new StringContent(JsonConvert.SerializeObject(processPaymentRequestDto), Encoding.UTF8, "application/json"));

            using (var responseBody = await response.Content.ReadAsStreamAsync())
            {
                return await JsonSerializer.DeserializeAsync<ProcessPaymentResponseDTO>(responseBody);
            }
        }

        private ProcessPaymentResponseDTO MockResponseFromClient()
        {
            var random = new Random();
            return new ProcessPaymentResponseDTO { Id = Guid.NewGuid(), Status = random.Next(0, 3).ToString()};
        }
    }
}
