﻿namespace Data.Gateway
{
    public class ProcessPaymentRequestDTO
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CardNumber { get; set; }

        public string Flag { get; set; }

        public int ExpiryMonth { get; set; }

        public int ExpiryYear { get; set; }

        public int CVV { get; set; }

        public string Address { get; set; }
    }
}
