﻿namespace Data.Gateway.IoC
{
    using System.Net.Http;
    using Autofac;
    using Microsoft.Extensions.Configuration;
    
    public class IoCModule : Module
    {
        private readonly IConfigurationSection clientSettings;

        public IoCModule(IConfigurationRoot configurationRoot)
        {
            this.clientSettings = configurationRoot.GetSection("BankClientSettings");
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HttpClient>();
            builder.RegisterType<BankClient>().WithParameter("clientConfiguration", this.clientSettings).As<IBankClient>();
        }
    }
}
