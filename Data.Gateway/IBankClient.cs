﻿namespace Data.Gateway
{
    using System.Threading.Tasks;
    using Domain.Model;

    public interface IBankClient
    {
        Task<ProcessPaymentResponseDTO> AddPaymentAsync(Payment payment);
    }
}
