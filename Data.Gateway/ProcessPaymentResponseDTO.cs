﻿namespace Data.Gateway
{
    using System;

    public class ProcessPaymentResponseDTO
    {
        public Guid Id { get; set; }

        public string Status { get; set; }
    }
}
