﻿namespace Presentation.WebAPI
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Application.Service.Profile;
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using AutoMapper;
    using Data.Repository;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.OpenApi.Models;
    using Presentation.WebAPI.Middlewares;
    using ApplicationModule = Application.Service.IoC;
    using GatewayModule = Data.Gateway.IoC;
    using RepositoryModule = Data.Repository.IoC;

    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; private set; }

        public ILifetimeScope AutofacContainer { get; private set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = "Payment Service API",
                        Version = "v1"
                    });
                });
            var mappingConfig = new MapperConfiguration(mc =>
                            {
                                mc.AddProfile(new PaymentProfile());
                            });

            var mapper = mappingConfig.CreateMapper();

            services.AddSingleton(mapper);
            services.AddDbContext<PaymentContext>(opt => opt.UseInMemoryDatabase("PaymentService"));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var builder = new ContainerBuilder();

            builder.Populate(services);

            builder.RegisterModule(new ApplicationModule.IoCModule());
            builder.RegisterModule(new RepositoryModule.IoCModule());
            builder.RegisterModule(new GatewayModule.IoCModule(this.Configuration));

            AutofacContainer = builder.Build();

            return new AutofacServiceProvider(AutofacContainer);
        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(this.Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            var logger = loggerFactory.CreateLogger("PaymentService");
            logger.LogInformation("Application Start");

            app.UseSwagger();
            app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Payment Service API V1");
                    c.RoutePrefix = string.Empty;
                });
            app.UseMiddleware(typeof(ExceptionHandlingMiddleware));
            app.UseMvc();
        }
    }
}
