﻿namespace Presentation.WebAPI.Controllers
{
    using System;
    using System.Net.Mime;
    using System.Threading.Tasks;
    using Application.DTO;
    using Application.Service;
    using Application.Service.Validator;
    using AutoMapper;
    using Domain.Model;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly IPaymentService paymentService;

        private readonly IMapper mapper;

        public PaymentsController(IPaymentService paymentService, IMapper mapper)
        {
            this.paymentService = paymentService;
            this.mapper = mapper;
        }

        /// <summary>
        /// GET Payment By Id
        /// </summary>
        /// <param name="id">Universally Unique Identifier of the payment</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PaymentResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var payment = await this.paymentService.GetAsync(id);

            if (payment == null)
            {
                return this.NotFound();
            }

            return this.Ok(payment);
        }

        /// <summary>
        /// POST Payment
        /// </summary>
        /// <param name="paymentRequest">Payment Request</param>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PostAsync([FromBody] PaymentRequestDto paymentRequest)
        {
            var paymentValidator = new PaymentValidator(paymentRequest);

            if (!paymentValidator.IsValid())
            {
                return this.BadRequest(paymentValidator.ErrorList);
            }

            var payment = this.mapper.Map<Payment>(paymentRequest);
            var result = await this.paymentService.ProcessAsync(payment);
            
            return this.CreatedAtAction(nameof(this.GetAsync), new { id = result.Id }, result.Id);
        }
    }
}
