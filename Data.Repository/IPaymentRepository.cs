﻿namespace Data.Repository
{
    using System;
    using System.Threading.Tasks;
    using Domain.Model;

    public interface IPaymentRepository
    {
        Task<bool> AddAsync(Payment payment);

        Task<Payment> GetAsync(Guid id);

        Task UpdateAsync(Payment payment);
    }
}
