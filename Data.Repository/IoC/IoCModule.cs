﻿namespace Data.Repository.IoC
{
    using System.Diagnostics.CodeAnalysis;
    using Autofac;

    [ExcludeFromCodeCoverage]
    public class IoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PaymentRepository>().As<IPaymentRepository>();
        }
    }
}
