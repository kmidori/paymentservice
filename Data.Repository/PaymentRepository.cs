﻿namespace Data.Repository
{
    using System;
    using System.Threading.Tasks;
    using Domain.Model;
    using Microsoft.EntityFrameworkCore;

    public class PaymentRepository : IPaymentRepository
    {
        private readonly PaymentContext context;

        public PaymentRepository(PaymentContext context)
        {
            this.context = context;
        }

        public async Task<bool> AddAsync(Payment payment)
        {
            this.context.Payments.Add(payment);
            var rows = await this.context.SaveChangesAsync();
            
            return rows > 1;
        }

        public Task UpdateAsync(Payment payment)
        {
            this.context.Entry(payment).State = EntityState.Modified;
            this.context.SaveChangesAsync();

            return Task.CompletedTask;
        }

        public async Task<Payment> GetAsync(Guid id)
        {
            return await this.context.Payments.FindAsync(id);
        }
    }
}
