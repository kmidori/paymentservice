﻿namespace Data.Repository
{
    using System.Diagnostics.CodeAnalysis;
    using Domain.Model;
    using Microsoft.EntityFrameworkCore;

    [ExcludeFromCodeCoverage]
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options)
            : base(options)
        {
        }

        public DbSet<Payment> Payments { get; set; }
    }
}
