namespace Presentation.WebAPI.Tests
{
    using System;
    using System.Threading.Tasks;
    using Application.DTO;
    using Application.Service;
    using Application.Service.Profile;
    using AutoFixture;
    using AutoMapper;
    using Domain.Model;
    using FluentAssertions;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Presentation.WebAPI.Controllers;
    using Xunit;

    public class PaymentsControllerTests
    {
        private readonly Fixture fixture;
        private readonly Mock<IPaymentService> paymentServiceMock;
        private readonly IMapper mapper;

        private readonly PaymentsController paymentsController;

        public PaymentsControllerTests()
        {
            var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<PaymentProfile>();
                });
            this.mapper = config.CreateMapper();

            this.fixture = new Fixture();
            this.paymentServiceMock = new Mock<IPaymentService>();
            this.paymentsController = new PaymentsController(this.paymentServiceMock.Object, this.mapper);
        }

        [Fact]
        public async Task GetAsync_WithValidGuid_ShouldReturnOk()
        {
            // Arrange
            var id = this.fixture.Create<Guid>();
            this.paymentServiceMock.Setup(s => s.GetAsync(id)).ReturnsAsync(this.fixture.Create<PaymentResponseDto>());

            // Act
            var result = await this.paymentsController.GetAsync(id);

            // Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        [Fact]
        public async Task GetAsync_WithInvalidGuid_ShouldReturnNotFound()
        {
            // Arrange
            var id = this.fixture.Create<Guid>();

            // Act
            var result = await this.paymentsController.GetAsync(id);

            // Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        [Fact]
        public async Task PostAsync_WithValidPayment_ShouldReturnCreatedResult()
        {
            // Arrange
            var paymentDto = this.fixture.Build<PaymentRequestDto>().With(w => w.CardNumber, "41970138204188855").Create();
            var paymentResponseDto = this.fixture.Create<PaymentResponseDto>();

            this.paymentServiceMock.Setup(s => s.ProcessAsync(It.Is<Payment>(p => p.CardNumber == paymentDto.CardNumber)))
                .ReturnsAsync(paymentResponseDto);

            // Act
            var result = await this.paymentsController.PostAsync(paymentDto);

            // Assert
            result.Should().BeOfType<CreatedAtActionResult>();
        }

        [Fact]
        public async Task PostAsync_WithValidPayment_ShouldCallProcessAsync()
        {
            // Arrange
            var paymentDto = this.fixture.Build<PaymentRequestDto>().With(w => w.CardNumber, "41970138204188855").Create();
            var paymentResponseDto = this.fixture.Create<PaymentResponseDto>();

            this.paymentServiceMock.Setup(s => s.ProcessAsync(It.Is<Payment>(f => f.CardNumber == paymentDto.CardNumber)))
                .ReturnsAsync(paymentResponseDto);

            // Act
            await this.paymentsController.PostAsync(paymentDto);

            // Assert
            this.paymentServiceMock.VerifyAll();
        }

        [Fact]
        public async Task PostAsync_WithInvalidPayment_ShouldReturnBadRequest()
        {
            // Arrange
            var paymentDto = this.fixture.Build<PaymentRequestDto>().With(w => w.CardNumber, "x").Create();
            
            // Act
            var result = await this.paymentsController.PostAsync(paymentDto);

            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }
    }
}
