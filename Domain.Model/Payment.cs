﻿namespace Domain.Model
{
    using System;

    public class Payment
    {
        public Guid Id { get; set; }

        public Guid ExternalId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CardNumber { get; set; }

        public string Flag { get; set; }

        public int ExpiryMonth { get; set; }

        public int ExpiryYear { get; set; }

        public int CVV { get; set; }

        public string Address { get; set; }

        public PaymentStatus Status { get; set; }
        
        public Payment ShallowCopy()
        {
            return (Payment)this.MemberwiseClone();
        }
    }
}
