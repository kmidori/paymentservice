﻿namespace Domain.Model
{
    public class PaymentWithoutSensitiveData
    {
        private readonly Payment payment;

        public PaymentWithoutSensitiveData(Payment payment)
        {
            this.payment = payment.ShallowCopy();
        }
        
        public Payment GetPayment()
        {
            this.payment.CardNumber = this.HideCardNumber();
            return this.payment;
        }

        private string HideCardNumber()
        {
            return string.Concat(string.Empty.PadLeft(12, '*'), this.payment.CardNumber.Substring(this.payment.CardNumber.Length - 4));
        }
    }
}
