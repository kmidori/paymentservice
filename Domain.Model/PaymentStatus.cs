﻿namespace Domain.Model
{
    public enum PaymentStatus
    {
        Pending,
        Success,
        Canceled,
        Rejected
    }
}
