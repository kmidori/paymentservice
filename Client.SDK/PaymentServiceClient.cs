﻿namespace Client.SDK
{
    using System;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Application.DTO;
    using Newtonsoft.Json;

    public class PaymentServiceClient
    {
        private readonly HttpClient httpClient;

        private readonly IClientConfiguration clientConfiguration;

        public PaymentServiceClient(HttpClient httpClient, IClientConfiguration clientConfiguration)
        {
            this.httpClient = httpClient;
            this.clientConfiguration = clientConfiguration;
        }

        public async Task<HttpResponseMessage> AddPayment(PaymentRequestDto paymentRequestDto)
        {
            return await this.httpClient.PostAsync(
                           this.GetFullPath("api/payments"),
                           new StringContent(JsonConvert.SerializeObject(paymentRequestDto), Encoding.UTF8, "application/json"));
        }

        public async Task<HttpResponseMessage> GetPayment(Guid id)
        {
            return await this.httpClient.GetAsync(this.GetFullPath($"api/payments/{id}"));
        }

        private Uri GetFullPath(string path)
        {
            return new Uri($"{this.clientConfiguration.Endpoint}{path}");
        }
    }
}
