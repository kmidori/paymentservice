﻿namespace Client.SDK
{
    public interface IClientConfiguration
    {
        string Endpoint { get; }
    }
}
