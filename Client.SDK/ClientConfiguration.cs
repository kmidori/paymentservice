﻿namespace Client.SDK
{
    public class ClientConfiguration : IClientConfiguration
    {
        public ClientConfiguration(string uri)
        {
            this.Endpoint = uri;
        }

        public string Endpoint { get; }
    }
}
